\section{Example 1: Fabry-Perot cavity}
\label{SEC-FABRY-PEROT}

A two-mirror Fabry-Perot caity is chosen for the first example in
order to confirm the well-known stability criteria using mirror
g-factors.

The ABCD matrix for a space with the length of $L$:
\begin{equation}
  {\cal S}(L) = \left(
    \begin{array}{cc}
      1 & L \\
      0 & 1 \\
    \end{array}
  \right)
\end{equation}

The ABCD matrix for a mirror with the curvature radius of $R$:
\begin{equation}
  {\cal F}(R) = \left(
    \begin{array}{cc}
      1 & 0 \\
      -2/R & 1 \\
    \end{array}
  \right)
\end{equation}

Let's assume we have a linear cavity with the length of $L$ and the
curvature radii of the input and end mirrors as $R_1$ and $R_2$. The
round-trip ABCD matrix is calculated as
\begin{align}
  \left(
    \begin{array}{cc}
      A & B \\
      C & D \\
    \end{array}
  \right)
  &=
  {\cal F}(R_1)\,{\cal S}(L)\,{\cal F}(R_2)\,{\cal S}(L)\\
  &=
  \left(
    \begin{array}{cc}
      1-\cfrac{2 L}{R_2} & 2 L -\cfrac{2 L^2}{R_2} \\
      -\cfrac{2}{R_2}-\cfrac{2}{R_1} + \cfrac{4 L}{R_1 R_2} & 1-\cfrac{2
        L}{R_2}-\cfrac{4 L}{R_1}+\cfrac{4 L^2}{R_1 R_2} \\
    \end{array}
  \right).
\end{align}

From this, we can calculate the cosine of the round-trip Gouy phase
shift as
\begin{align}
  \cos\zeta &= \frac{A+D}{2} \\
  & = 1 - 2 \frac{L}{R_1} - 2 \frac{L}{R_2} + 2 \frac{L}{R_1}\frac{L}{R_2} \\
  & = 2 \left(1 - \frac{L}{R_1}\right)\left(1 - \frac{L}{R_2}\right) - 1 \\
  & \equiv 2 g_1 g_2 - 1
\end{align}
or the quantity equivalent to the product of the g-factors
\begin{align}
  \cos^2 \frac{\zeta}{2} &= \frac{A+D+2}{4}  \\
  & = \left(1 - \frac{L}{R_1}\right)\left(1 - \frac{L}{R_2}\right)  \\
  & \equiv g_1 g_2
\end{align}

The sign of the g-factor can be extracted from the element $B$:
\begin{align}
  \sgn B &= \sgn \left[ 2L \left(1-\cfrac{L}{R_2}\right)\right]\\
  & = \sgn\,g_2
\end{align}
Therefore we obtain
\begin{equation}
  \zeta = 2 \cos^{-1} (\sgn\,g_2\,\cdot\,\sqrt{g_1 g_2}).
\end{equation}
This is equivalent to the famous expression Eq.~(\ref{EQN-G-GOUY-PHASE}).

\section{Example 2: aLIGO OMC}

The second example is the aLIGO output mode cleaner (OMC). The cavity
of the OMC is a bow-tie style ring cavity formed by four mirrors: two
flat input/output mirrors (FMs) and two curved mirrors (CMs) with the
curvature radius of $R = 2.575 {\rm m}$ (as the average number of
measured values). The cavity mirrors are arranged in the order of
FM1-FM2-CM1-CM2-FM1.  The distances FM1-FM2 and CM1-CM2 are both $L_1
= 0.2816\,{\rm m}$, while the distances FM2-CM1, and CM2-FM1 are both
$L_2 = 0.2844\,{\rm m}$. This means the round-trip length is $\LRT =
1.132\,{\rm m}$, and the FSR is $\nu_{\rm FSR} = 264.8\,{\rm MHz}$. The
angle of incidence on each mirror is $\theta = 4.042\,{\deg}\,(= 0.0706\,
{\rm rad})$.

The round-trip ABCD matrix ${\cal C}$ is defined as
\begin{equation}
{\cal C}_{\pm} = 
{\cal M}_{\pm}\,{\cal S}(L_2)
{\cal M}_{\pm}\,{\cal F}(R_\pm)\,{\cal S}(L_1)
{\cal M}_{\pm}\,{\cal F}(R_\pm)\,{\cal S}(L_2)
{\cal M}_{\pm}\,{\cal S}(L_1).
\end{equation}
Because of the non-zero incident angle, the system naturally involves
astigmatism.  The signs $+$ and $-$ are applied to the vertical and
horizontal mode, respectively. The reflection matrix ${\cal M}_{\pm}$
is defined as
\begin{equation}
{\cal M}_{\pm} = 
\left(
  \begin{array}{cc}
    \pm 1&0\\
    0 & \pm 1
  \end{array}
\right),
\end{equation}
which correspond to the phase flip of the horizontal modes when the
beam is reflected with non-zero incident angle in the horizontal
plane. This matrix becomes important for an odd-number-mirror ring
cavity, while we can ignore them for our four mirror cavity. The
curved mirror therefore behaves as an astigmatic mirror with an
effective radius of curvature of
\begin{equation}
R_{\pm} = R\,\cdot\,(\cos\theta)^{\pm 1}\,\,\,.
\end{equation}

The results of the mode calculations are shown below.

\subsubsection*{Horizontal mode}

\begin{equation}
{\cal C}_{-} = 
\left(
  \begin{array}{cc}
    0.3892 & 0.7242\\
    -1.381 & 0.0004690
  \end{array}
\right)
\end{equation}
Thus we obtain
\begin{align}
\zeta & = \sgn{B}\,.\,\cos^{-1} \frac{A+D}{2} = 1.375\,{\rm rad}
\Rightarrow 39.38\,\deg
\end{align}
\begin{equation}
\nu_{\rm TMS,H} = \frac{\zeta}{2 \pi}\,\nu_{\rm FSR} 
= 0.2188\,\nu_{\rm FSR} = 57.94 {\rm MHz}
\end{equation}

\subsubsection*{Vertical mode}
\begin{equation}
{\cal C}_{+} = 
\left(
  \begin{array}{cc}
    0.3864 & 0.7223\\
    -1.387 & -0.0004043
  \end{array}
\right)
\end{equation}
Thus we obtain
\begin{align}
\zeta & = \sgn{B}\,\cdot\,\cos^{-1} \frac{A+D}{2} = 1.378\,{\rm rad}
\Rightarrow 39.49\,\deg
\end{align}
\begin{equation}
\nu_{\rm TMS,V} = \frac{\zeta}{2 \pi}\,\nu_{\rm FSR} 
= 0.2194\,\nu_{\rm FSR} = 58.10 {\rm MHz}
\end{equation}

\section{Example 3: Rollins conjecture}

J.~Rollins conjectured that the accumulated round-trip Gouy phase
shift $\zeta$ of a multi-mirror cavity can be approximated by the
product of the generalized g-factors $g_i$, as follows:
\begin{align}
  \zeta &= 2 \cos^{-1} \sqrt{\prod_{i=1}^n g_i}\\
  g_i & \equiv 1 - \frac{\LRT}{2 R_i}
\end{align}
where $\LRT$ is the round-trip length of the cavity,
and $R_i$ is the curvature radius of the {$i$-th} mirror.

The first guess is that if all of ${\delta}g_i = 1-g_i$ are small enough,
$\displaystyle\frac{A+D+2}{4}$ may give us a good approximation of the
the generalized g-factor product. Let's prove it in the following subsections.

In the following derivation it is convenient to define the ABCD matrix of a curved
mirror and a space as
\begin{equation}
{\cal P}_i = 
{\cal F}(R_i)\,{\cal S}(L_i) = 
\left(
  \begin{array}{cc}
    1&L_i\\
    -\cfrac{2}{R_i}& 1-\cfrac{2 L_i}{R_i}
  \end{array}
\right)
=
\left(
  \begin{array}{cc}
    1&L_i\\
    -\cfrac{{\delta}f_i}{\LRT}&1-\cfrac{{\delta}f_i}{\LRT} L_i
  \end{array}
\right)
\end{equation}
where
\begin{equation}
{\delta}f_i \equiv \frac{2 \LRT}{R_i} = 4 {\delta}g_i
\end{equation}

\subsubsection*{Proposition:}
The ABCD matrix of the optical system 
${\cal P}_{n}{\cal P}_{n-1}\ldots{\cal P}_{1}$
is represented in the following form for $n \ge 2$, up to the first order of
${\delta}f_i$.
\begin{equation}
{\cal P}_{n}{\cal P}_{n-1}\ldots{\cal P}_{1} =
\left(
  \begin{array}{cc}
    A_n & B_n \\
    C_n & D_n \\
  \end{array}
\right),
\end{equation}
where
\begin{align}
 A_n & = 1 - \sum_{i=2}^{n}\left(L_i \sum_{j=1}^{i-1} \frac{{\delta}f_j}{\LRT}
\right) \label{EQN-AN}\\
B_n &= \sum_{i=1}^n L_i 
- \sum_{i=1}^{n-1} 
\left[\frac{{\delta}f_i}{\LRT}
  \left(\sum_{j=1}^i L_j\right)
  \left(\sum_{k=i+1}^n L_k\right)
\right]\\
C_n & = - \sum_{i=1}^n \frac{{\delta}f_i}{\LRT}\\
D_n &= 1 - \sum_{i=1}^n \left(\frac{{\delta}f_i}{\LRT} \sum_{j=1}^i
  L_j\right) \label{EQN-DN}
\end{align}

\subsubsection*{Proof:}

For $n=2$, we directly calculate ${\cal P}_{2}{\cal P}_{1}$:
\begin{align}
{\cal P}_{2}{\cal P}_{1} &=
\left(
  \begin{array}{cc}
    1&L_2\\
    -\cfrac{{\delta}f_2}{\LRT}& 1-\cfrac{{\delta}f_2 L_2}{\LRT}
  \end{array}
\right)
\left(
  \begin{array}{cc}
    1&L_1\\
    -\cfrac{{\delta}f_1}{\LRT}& 1-\cfrac{{\delta}f_1 L_1}{\LRT}
  \end{array}
\right)\\
&=\left(
  \begin{array}{cc}
    1 - L_2 \cfrac{{\delta}f_1}{\LRT} & L_1 + L_2 - \cfrac{{\delta}f_1}{\LRT} L_1 L_2\\
    -\cfrac{{\delta}f_1+{\delta}f_2}{\LRT}& 1-\cfrac{{\delta}f_1}{\LRT} L_1 -
    \cfrac{{\delta}f_2}{\LRT}(L_1+L_2)
  \end{array}
\right)  +{\cal O}^2({\delta}f_i)
\end{align}

Assuming the proposition is fulfilled for $A_n$, $B_n$, $C_n$ and $D_n$,
we calculate ${\cal P}_{n+1}{\cal P}_{n}\ldots{\cal P}_{1}$ as
\begin{align}
{\cal P}_{n+1}{\cal P}_{n}\ldots{\cal P}_{1} &=
\left(
  \begin{array}{cc}
    1&L_{n+1}\\
    -\cfrac{{\delta}f_{n+1}}{\LRT}& 1-\cfrac{{\delta}f_{n+1} L_{n+1}}{\LRT}
  \end{array}
\right)
\left(
  \begin{array}{cc}
    A_n & B_n \\
    C_n & D_n \\
  \end{array}
\right) +{\cal O}^2({\delta}f_i)\\
&=\left(
  \begin{array}{cc}
    A_n - L_{n+1} \sum_{i=1}^n \cfrac{{\delta}f_i}{\LRT} 
    &
    B_n + L_{n+1} \left[1 - \sum_{i=1}^n \left(L_i \sum_{j=i}^n \cfrac{{\delta}f_j}{\LRT}\right)\right]\\
    C_n - \cfrac{{\delta}f_{n+1}}{\LRT}
    & 
    D_n - \cfrac{{\delta}f_{n+1}}{\LRT}\sum_{i=1}^n L_i
  \end{array}
\right)  +{\cal O}^2({\delta}f_i)\\&=\left(
  \begin{array}{cc}
    A_{n+1} & B_{n+1} \\
    C_{n+1} & D_{n+1}
  \end{array}
\right)  +{\cal O}^2({\delta}f_i)
\end{align}
Q.E.D.

\subsubsection*{Consequence}

From Eqs.~(\ref{EQN-AN}) and (\ref{EQN-DN}), we obtain 
\begin{align}
  \frac{A+D+2}{4}\,\, &(= \cos^2 \frac{\zeta}{2})  \\
& = 1 - \sum_{i=1}^n \left[\cfrac{{\delta}f_i}{4 \LRT}
    \left(\sum_{j=1}^n L_i \right)\right]+ O^2({\delta}f_i)  \\
  & = 1 - \sum_{i=1}^n \frac{{\delta}f_i}{4}+ O^2({\delta}f_i)  \\
  & = 1 - \sum_{i=1}^n \frac{\LRT}{2 R_i}+ O^2({\delta}f_i) \label{EQN-AD24-ROLLINS1}
\end{align}
This quantity can be compared with the product of the generalized g-factors.
\begin{align}
  \prod_{i=1}^n g_i & = \prod_{i=1}^n \left(1-\frac{\LRT}{2 R_i}\right)\\
  & = \prod_{i=1}^n \left(1-\frac{{\delta}f_i}{4}\right)\\
  & = 1- \sum_{i=1}^n \frac{{\delta}f_i}{4} + O^2({\delta}f_i)\\
  & = 1- \sum_{i=1}^n \frac{\LRT}{2 R_i} + O^2({\delta}f_i)
\end{align}
This agrees with Eq.~(\ref{EQN-AD24-ROLLINS1}).

This means that {\bf Rollins conjecture is true if all of the
  curvature radii of the mirrors are long enough relative to the
  round-trip length of the cavity} (i.e. ${\delta}g_i \ll 1$ for all
$i$).  Note that this result means that permutation of the mirrors
does not change the resulting round-trip Gouy phase significantly up
to the first order in this small ${\delta}g_i$ regime. Also note that
all of the g-factors are kept in the positive number. So there is no
ambiguity of the Gouy phase shift.

