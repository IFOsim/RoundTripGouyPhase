\section{Derivation}

The derivation of Eqs.~(\ref{EQN-GOUY-PHASE1}) and
(\ref{EQN-GOUY-PHASE2}) is based on the following two points:
\begin{enumerate}

\item The accumulated Gouy phase shift between an arbitrary two points
  of a Gaussian beam can be computed only from the ABCD matrix between
  the points and the parameter of the input beam. Since the detail of
  this point is proven elsewhere~\cite{BIB-ERDEN}, we will use this
  result without re-derivation.

\item The beam parameters of a cavity eigenmode id necessarily
  reproduced after a round trip in the cavity. Therefore, the cavity
  eigenmode must fulfill the beam-reproducing condition $\qout =
  \qin$. This means that the input and output beams have the same
  waist radii, same waist positions, and thus the same wavefront
  radii of curvature.
\end{enumerate}

Erden et al.~\cite{BIB-ERDEN} described how the beam parameters
(i.e. the beam radius, wave front radius of curvature, and Gouy phase)
are described by the elements of the ABCD matrix and the parameters of
the input beam Eqs.~(13)$\sim$(15) in \cite{BIB-ERDEN}. Here are the
excerpted equations rewritten:
\begin{equation}
\wout^2 = 
\win^2 \left(A+\frac{B}{\rin}\right)^2+\frac{B^2 \lambda^2}{\pi^2 \win^2}
\end{equation}

\begin{equation}
\frac{1}{\rout} = 
\frac{\left(C + \cfrac{D}{\rin}\right)\left(A+\cfrac{B}{\rin}\right)+\cfrac{B D \lambda^2}{\pi^2 \win^4}}
{\left(A+\cfrac{B}{\rin}\right)+\cfrac{B^2 \lambda^2}{\pi^2 \win^4}}
\end{equation}

\begin{equation}
\tan {\zeta} = 
\frac{B}{\left(A+\cfrac{B}{\rin}\right) \cfrac{\pi \omega^2_{\rm in}}{\lambda}}\label{EQN-TAN-ZETA}
\end{equation}

Note that the ABCD matrix in~\cite{BIB-ERDEN} is defined differently
from the equations above. The equations here have been modified so
that the convention of the elements $A,\,B,\,C,\,D$ agrees with the
usual conventions in the optics book like~\cite{BIB-SIEGMAN}. 
(i.e. $B_{\rm Siegman} = B_{\rm Erden} / \lambda, 
C_{\rm Siegman} = C_{\rm Erden} \lambda$ )


Indeed, the consequence of their paper, shown as
Eq.~(\ref{EQN-TAN-ZETA}) here, is a useful expression. This expression
enables us to calculate the accumulated Gouy phase shift, ${\zeta}$,
from $\rin$, $\win$ and the elements of the ABCD matrix, no matter
what optical elements are in the path. This may be particularly useful
for the calculation of $\zeta$ for the Gouy phase telescope of the
wave front sensing (WFS) systems.

Erden et al. conversely derived the elements of the ABCD matrix from
the beam parameters.
\begin{align}
A & = \frac{\wout}{\win} \cos \zeta - \frac{B}{\rin}\\
B & = \frac{\pi \win \wout}{\lambda} \sin \zeta\\
C & = \frac{A}{\rout} 
- \frac{\cfrac{1}{\rin}\left(A+\cfrac{B}{\rin}\right)+\cfrac{B \lambda^2}{\pi^2 \win^4}}
{\left(A+\cfrac{B}{\rin}\right)^2+\cfrac{B^2 \lambda^2}{\pi^2 \win^4}}\\
D & = \frac{1 + B C}{A}
\end{align}
The last equation comes from the unitarity of the ABCD matrix.

After simplification, the quantity $\displaystyle\frac{A+D}{2}$ can be
expressed as follows
\begin{equation}
\frac{A+D}{2}
= \frac{1}{2} \left(\frac{\win}{\wout}+\frac{\wout}{\win}\right)\cos\zeta 
+ \frac{1}{2} \left(\frac{1}{\rout}-\frac{1}{\rin}\right)\frac{\pi \win \wout}{\lambda} \sin\zeta
\end{equation}
For the round-trip Gouy phase shift of a cavity, the formula is
significantly simplified because of the beam-reproducing
condition. We therefore find
\begin{equation}
\frac{A+D}{2} = \cos\zeta 
\end{equation}
and
\begin{equation}
\frac{A+D+2}{4} = \frac{\cos\zeta + 1}{2} = \cos^2\frac{\zeta}{2}\label{EQN-AD24}
\end{equation}

Because of the multi-value nature of the inverse cosine function 
there is a sign ambiguity:
\begin{equation}
\cos(|\zeta|) = \cos(-|\zeta|) 
\end{equation}
The key to solve this ambiguity is the element $B$.
As the sign of $B$ is determined by $\sin \zeta$, ${\rm sgn} B$
indicates the sign of $\zeta$. Therefore we obtain the following expressions
\begin{equation}
\zeta = {\rm sgn}{B}\,\cdot\,\cos^{-1}\left(\frac{A+D}{2}\right)\,\,\,,
\end{equation}
where the value range of $\cos^{-1} \zeta$ is
\begin{equation}
0 \le \cos^{-1} \zeta < 2 \pi\,\,\,(-1 < \zeta \le 1).
\end{equation}
If we use the inverse function of Eq.~({\ref{EQN-AD24}}) for the value
range above, we find $\cos\cfrac{\zeta}{2}$ is always positive and the
ambiguity is hidden in the sign of the square-root term. Therefore the
sign needs to be inside of the inverse cosine:
\begin{equation}
\zeta = 2 \cos^{-1}\left({\rm sgn}{B}\,\cdot\,\sqrt{\frac{A+D+2}{4}}\right)\,\,\,.
\end{equation}
