\section{Introduction}

It is well known that the stability of a two-mirror Fabry-Perot cavity
can be characterized by the product of two values called g-factors:
\begin{align}
g_1 & = 1-\frac{L}{R_1}\\
g_2 & = 1-\frac{L}{R_2},
\end{align}
where $L$ is the length of the cavity, and $R_1$ and $R_2$ are the
curvature radii of the cavity mirrors. When the product $g_1 g_2$
fulfills the following condition, the cavity has stable eigenmodes.
\begin{equation}
  0 \le g_1 g_2 \le 1 \label{EQN-G-STABILITY}
\end{equation}

The product of the g-factors also provides a handy way to calculate
the transverse mode spacing (TMS) of the cavity. The product of the
g-factors is closely related to the accumulated round-trip Gouy phase
shift $(\zeta)$ between the cavity eigenmodes\footnote{The word
  ``accumulated Gouy phase shift'' means the shift of the optical
  phase between one of the Gaussian modes and the mode at the next
  higher-order spacial mode (e.g. ${\rm TEM}_{00}$ and ${\rm
    TEM}_{01}$) caused by Gouy phase effect. The ``round-trip'' one is
  the same quantity for a single round-trip of the optical cavity.}.
\begin{equation}
  \zeta = 2 \cos^{-1} \pm \sqrt{g_1 g_2}\label{EQN-G-GOUY-PHASE}
\end{equation}
Here the sign $\pm$ is determined by the sign of $g_1$, and in fact
$g_1$ and $g_2$ have the same sign for a stable cavity
(cf. Eq.~(\ref{EQN-G-STABILITY})).  Consequently, the resulting TMS is given
by the following formula
\begin{equation}
  \nu_{\rm TMS} = \frac{\zeta}{2 \pi} \nu_{\rm FSR} 
  = \frac{\cos^{-1}\pm\sqrt{g_1 g_2}}{\pi}\nu_{\rm FSR} \label{EQN-TMS}
\end{equation}
where $\nu_{\rm FSR}$ is the free spectral range of the cavity that is
given by $\nu_{\rm FSR} = c/(2 L)$.


For a general cavity case (like a ring caity or a folded cavity with
intra-cavity lensing optics), one would naturally imagine that there
might be a similar simple indicator to the g-factors. In fact,
J.~Rollins recently conjectured from the discussion of the folded
recycling cavity of the 40m prototype that $\zeta$ can be approximately
calculated with the product of the generalized g-factors
\begin{align}
\zeta &= 2 \cos^{-1} \sqrt{\prod_{i=1}^n g_i}\\
g_i & = 1 - \frac{L_{\rm RT}}{2 R_i}
\end{align}
where $L_{\rm RT}$ is the round-trip length of the cavity, and $R_i$
is the curvature radius of the {i-th} mirror.  However, {\bf this
  conjecture is obviously not applicable to every cavity}; N.~Smith
pointed out that permutation of the mirrors in a ring cavity (like the
output mode cleaner cavity) changes the TMS while the corresponding
permutation of the g-factors in the product does not change
$\zeta$. In fact, it will be shown in this document that {\bf this
  conjecture is useful only when $1-g_i \ll 1$ for all $i$}.
\footnote{Therefore, the product of the generalized g-factors is
  always positive. There is no sign ambiguity.}

As long as the simple and exact indicator like the g-factors is not
available, we need to go back to the first principle of the beam
calculation: ABCD matrix. If we calculate the ABCD matrix of the
cascaded optical system by multiplying the individual ABCD matrices,
all aspect of the system in terms of the beam parameter can be
characterized. This can be understood by looking at the formula for
Gaussian beam transformation by the ABCD matrix that is derived from
Huygens propagarion integral (\cite{BIB-SIEGMAN}, Chapter~20):
\begin{equation}
\qout = \frac{A \qin + B}{C \qin + D}, \label{EQN-Q-ABCD}
\end{equation}
where $\qin$ and $\qout$ are the q-parameters for the input and output
beam respectively. $A$, $B$, $C$, and $D$ are the elements of the ABCD
matrix of the optical system. This formula means that different
optical systems with the same ABCD matrix result in identical beam
transformations.

It is also known that the quantity $(A+D)/2$ is closely related to
cavity stability. The cavity eigenmodes are supposed to fulfill the
beam-reproducing condition $\qout = \qin$. By solving this condition,
Eq.(\ref{EQN-Q-ABCD}) with the unitarity condition (i.e. $AD-BC = 1$),
we obtain two solutions for the forward and backward beams:
\begin{equation}
 q = \frac{A-D \pm \sqrt{(A+D)^2-4}}{2C}
\end{equation}
As $q$ is a complex number%
\footnote{cf. $q = z + {\rm i} z_{\rm R}$, where $z_{\rm R}$ is the
  Rayleigh range.}, the stability criteria is given by
\begin{equation}
 -1 \le \frac{A+D}{2} \le 1.  \label{EQN-ABCD-STABILITY1}
\end{equation}
Equivalently, this can be expressed as
\begin{align}
 0 \le \frac{A+D+2}{4}\le 1.  \label{EQN-ABCD-STABILITY2}
\end{align}
As we will find in Section~\ref{SEC-FABRY-PEROT}, 
$\displaystyle\frac{A+D+2}{4}$ for a Fabry-Perot cavity
corresponds to the product of the g-factors $g_1 g_2$.
Therefore Eq.~(\ref{EQN-ABCD-STABILITY2}) is equivalent to Eq.~(\ref{EQN-G-STABILITY}).

In this note, the interpretation of Eqs.~(\ref{EQN-ABCD-STABILITY1})
and (\ref{EQN-ABCD-STABILITY2}) are extended for general cavities.  It
is derived that {\bf the accumulated round-trip Gouy phase shift can
  be computed only from the \underline{round-trip} ABCD matrix of the
  cavity} as:
\begin{equation}
\zeta = {\rm sgn}{B}\,\cdot\,\cos^{-1}\left(\frac{A+D}{2}\right)\,\,\,,\label{EQN-GOUY-PHASE1}
\end{equation}
where the value range of $\cos^{-1} x$ is defined to be 
\begin{equation}
0 \le \cos^{-1} x < 2 \pi\,\,\,(-1 < x \le 1).
\end{equation}
Eq.~(\ref{EQN-GOUY-PHASE1}) can equivalently be expressed as 
\begin{equation}
\zeta = 2 \cos^{-1}\left({\rm sgn}{B}\,\cdot\,\sqrt{\frac{A+D+2}{4}}\right)\,\,\,.\label{EQN-GOUY-PHASE2}
\end{equation}
These expressions requires the calculation of the ABCD matrix for the
round-trip path, and therefore more complicated to calculate than the
product of the g-factors. However, it still provides concise
evaluation of the stability on the general cavities.

