#!/usr/bin/make -f

.PHONY: all
all: cavity_gouy_phase

cavity_gouy_phase: %: %.tex
	pdflatex $(MODE) $@
#	bibtex $(MODE) $@
	pdflatex $(MODE) $@
	pdflatex $(MODE) $@

.PHONY: clean
clean:
	rm -f cavity_gouy_phase.pdf *.aux *.dvi *.lof *.log *.lot *.out *.ps *.toc *.blg *.bbl
